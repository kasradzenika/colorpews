﻿using UnityEngine;
using System.Collections;

public class PlayerController : DestinationController
{
    public Animator animator;
    public float xSpeed = 3f;
    public float padding = 1f;
    public bool isLaunched = false;
    private float movXRange = 0f;
    private Transform _transform;
    private Vector3 _pos;
    private bool didStartGame = false;

    public static PlayerController instance;
    protected void Awake()
    {
        instance = this;
        movXRange = LevelController.instance.screenSizeInUnits.x * 0.5f;
        _transform = transform;
        _pos = _transform.localPosition;

        //make the first destination the same as the middle color
        SetDestinationType(DestinationsController.instance.destinations[2].destType);
    }

    private void Update()
    {
        //move on X axis while not launched
        if (!isLaunched && didStartGame)
        {
            if ((_pos.x > movXRange && xSpeed > 0f) ||  //too far right, go back
                (_pos.x < -movXRange && xSpeed < 0f))   //too far left, go back
                xSpeed *= -1f;

            _pos.x += xSpeed * Time.deltaTime;
            _transform.localPosition = _pos;
        }
    }

    public void Launch()
    {
        if(!isLaunched)
            StartCoroutine(LaunchCoroutine());
    }
    
    private IEnumerator LaunchCoroutine()
    {
        didStartGame = true;

        //NOTE isLaunched is marked true by the animator
        animator.SetTrigger("Launch");

        //wait until the animation starts
        yield return new WaitForSeconds(0.1f);

        //wait for the animation to end
        yield return new WaitWhile(() => isLaunched);

        ResetDestinationType();

        //NOTE isLaunched is reset by the animator
        yield break;
    }

    private void ResetDestinationType()
    {
        DestinationType dt = DestinationType.MISSED;
        while (dt == DestinationType.MISSED ||
            DestinationsController.instance.availableDestTypes.Contains(dt))
        {
            dt = (DestinationType)Random.Range(0, 6);
        }

        SetDestinationType(dt);
    }
}
