﻿using UnityEngine;
using System.Collections;

public class DestinationController : MonoBehaviour
{
    public MeshRenderer mRenderer;
    public DestinationType destType
    {
        get;
        private set;
    }

    public void SetDestinationType(DestinationType dType)
    {
        destType = dType;
        mRenderer.material = DestinationsController.instance.destinationMaterials[(int)dType];
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            bool success = destType == PlayerController.instance.destType;
            LevelController.instance.ReportShot(success);

            if(success)
                DestinationsController.instance.DidHit(this);
        }
    }
}

public enum DestinationType
{
    Blue = 0,
    Green,
    Orange,
    Red,
    Purple,
    Yellow,
    MISSED = 100  // <-- obstacles
}