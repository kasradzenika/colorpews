﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelController : MonoBehaviour
{
    public Text scoreText;
    public int score;
    public float paddingInUnits = 1f;

    [HideInInspector]
    public Vector2 screenSizeInUnits;

    public static LevelController instance;
    private void Awake()
    {
        instance = this;

        screenSizeInUnits = new Vector2(
            Camera.main.orthographicSize * Camera.main.aspect * 2f - paddingInUnits,
            Camera.main.orthographicSize * 2f - paddingInUnits
            );

        scoreText.text = score.ToString();
    }

    public void ReportShot(bool didHit)
    {
        if (didHit)
        {
            score++;
        }
        else
        {
            //score--;
            //if (score < 0) score = 0;
        }

        scoreText.text = score.ToString();
    }
}
