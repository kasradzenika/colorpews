﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour
{
    public static InputController instance;
    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            PlayerController.instance.Launch();
        }
    }
}
