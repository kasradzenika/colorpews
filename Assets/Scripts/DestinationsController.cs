﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestinationsController : MonoBehaviour
{
    public List<DestinationController> destinations;
    public Material[] destinationMaterials;
    public float updateSpeed = 3f;
    public Transform leftSpawnPoint;
    public Transform rightSpawnPoint;

    private Coroutine updateDestinationsCoroutine;
    [HideInInspector]
    public List<DestinationType> availableDestTypes;

    public static DestinationsController instance;
    private void Awake()
    {
        instance = this;

        availableDestTypes = new List<DestinationType>();
        availableDestTypes.Add(DestinationType.Blue);
        availableDestTypes.Add(DestinationType.Green);
        availableDestTypes.Add(DestinationType.Orange);
        availableDestTypes.Add(DestinationType.Purple);
        availableDestTypes.Add(DestinationType.Red);
        availableDestTypes.Add(DestinationType.Yellow);

        int dIndex;
        DestinationType dType;
        for (int i = 0; i < destinations.Count; i++)
        {
            dIndex = Random.Range(0, availableDestTypes.Count);
            dType = availableDestTypes[dIndex];
            availableDestTypes.RemoveAt(dIndex);

            destinations[i].SetDestinationType(dType);
        }
    }

    private void Start()
    {
        UpdateDestinations();
    }

    //get out destHit, and add it back from the outside of the screen
    public void DidHit(DestinationController destHit)
    {
        int destHitIndex = destinations.IndexOf(destHit);

        //switch type from available types
        availableDestTypes.Add(destHit.destType);
        int adtIndex = Random.Range(0, availableDestTypes.Count);
        destHit.SetDestinationType(availableDestTypes[adtIndex]);
        availableDestTypes.RemoveAt(adtIndex);

        destinations.Remove(destHit);
        if(destHitIndex <= destinations.Count / 2)
        {
            destHit.transform.localPosition = leftSpawnPoint.localPosition;
            destinations.Insert(0, destHit);
        }
        else
        {
            destHit.transform.localPosition = rightSpawnPoint.localPosition;
            destinations.Add(destHit);
        }

        UpdateDestinations();
    }

    private void UpdateDestinations()
    {
        if (updateDestinationsCoroutine != null)
            StopCoroutine(updateDestinationsCoroutine);

        updateDestinationsCoroutine = StartCoroutine(UpdateDestinationsCoroutine());
    }

    private IEnumerator UpdateDestinationsCoroutine()
    {
        Vector3[] curPoses = new Vector3[destinations.Count];   //current position
        Vector3[] destPoses = new Vector3[destinations.Count];  //desetination position

        //initialization loop
        for (int i = 0; i < destinations.Count; i++)
        {
            curPoses[i] = destinations[i].transform.localPosition;
            destPoses[i] = Vector3.right * (
                LevelController.instance.screenSizeInUnits.x * i / (destinations.Count - 1) -
                LevelController.instance.screenSizeInUnits.x * 0.5f
                );
        }

        yield return new WaitForEndOfFrame();

        float diff = 10f;
        float maxDiff = 10f;

        //animation loop
        while (maxDiff > 0.01f)
        {
            maxDiff = diff = 0f;
            for (int i = 0; i < destinations.Count; i++)
            {
                curPoses[i] = Vector3.Lerp(curPoses[i], destPoses[i], updateSpeed * Time.deltaTime);
                destinations[i].transform.localPosition = curPoses[i];

                diff = Mathf.Abs(curPoses[i].x - destPoses[i].x);
                maxDiff = (diff > maxDiff) ? diff : maxDiff;
            }

            yield return new WaitForEndOfFrame();
        }

        //finalize
        updateDestinationsCoroutine = null;
        yield break;
    }
}
